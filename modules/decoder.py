import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


from .attention import Attention
from .zoneout import ZoneoutLSTMCell


class ConvNorm(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1,
                 padding=None, dilation=1, bias=True, w_init_gain='linear'):
        super(ConvNorm, self).__init__()
        if padding is None:
            assert(kernel_size % 2 == 1)
            padding = int(dilation * (kernel_size - 1) / 2)
        self.conv = nn.Conv1d(in_channels, out_channels,
                                    kernel_size=kernel_size, stride=stride,
                                    padding=padding, dilation=dilation,
                                    bias=bias)
        nn.init.xavier_uniform_(
            self.conv.weight, gain=nn.init.calculate_gain(w_init_gain))

    def forward(self, signal):
        conv_signal = self.conv(signal)
        return conv_signal


class PreNet(nn.Module):
    def __init__(self, hp, channels, in_dim, depth):
        super().__init__()
        sizes = [in_dim] + [channels] * depth
        self.hp = hp
        self.layers = nn.ModuleList(
            [nn.Linear(in_size, out_size)
                for (in_size, out_size) in zip(sizes[:-1], sizes[1:])])

    # in default tacotron2 setting, we use prenet_dropout=0.5 for both train/infer.
    # you may want to set prenet_dropout=0.0 for some case.
    # remove prenet dropout, and using BatchNorm
    def forward(self, x, prenet_dropout):
        for linear in self.layers:
            x = F.dropout(F.relu(linear(x)), p=prenet_dropout, training=True)
        return x


class PostNet(nn.Module):
    def __init__(self, channels, kernel_size, n_mel_channels, depth):
        super().__init__()
        padding = (kernel_size - 1) // 2
        self.cnn = list()
        self.cnn.append(
            nn.Sequential(
                nn.Conv1d(n_mel_channels, channels, kernel_size=kernel_size, padding=padding),
                nn.BatchNorm1d(channels),
                nn.Tanh(),
                nn.Dropout(0.5), ))

        for i in range(1, depth - 1):
            self.cnn.append(
                nn.Sequential(
                    nn.Conv1d(channels, channels, kernel_size=kernel_size, padding=padding),
                    nn.BatchNorm1d(channels),
                    nn.Tanh(),
                    nn.Dropout(0.5), ))

        self.cnn.append(
            nn.Sequential(
                nn.Conv1d(channels, n_mel_channels, kernel_size=kernel_size, padding=padding), ))

        self.cnn = nn.Sequential(*self.cnn)

    def forward(self, x):
        return self.cnn(x)


class TacotronDecoder(nn.Module):
    def __init__(self, hp):
        super().__init__()
        self.hp = hp
        self.go_frame = nn.Parameter(
            torch.randn(1, hp.audio.n_mel_channels), requires_grad=True)
        self.n_frames_per_step = hp.train.n_frames_per_step
        self.n_mel_channels = hp.audio.n_mel_channels
        self.prenet = PreNet(
            hp, hp.chn.prenet, in_dim=hp.audio.n_mel_channels, depth=hp.depth.encoder)
        self.postnet = PostNet(
            hp.chn.postnet, hp.ker.postnet, hp.audio.n_mel_channels, hp.depth.postnet)
        self.encoder_embedding_dim = hp.chn.encoder + hp.chn.speaker + hp.token_embedding_size
        """
        self.attention_rnn = ZoneoutLSTMCell(
            hp.chn.prenet + hp.chn.encoder + hp.chn.speaker, hp.chn.attention_rnn, zoneout_prob=0.1)
        """
        self.attention_rnn = nn.LSTMCell(hp.chn.prenet + hp.prenet_f0_dim + self.encoder_embedding_dim, hp.chn.attention_rnn)
        self.attention_layer = Attention(
            hp.chn.attention_rnn, self.encoder_embedding_dim,
            hp.chn.attention, hp.chn.attention_location_n_filters,
            hp.chn.attention_location_kernel_size)
        """
        self.decoder_rnn = ZoneoutLSTMCell(
            hp.chn.attention_rnn + hp.chn.encoder + hp.chn.speaker, hp.chn.decoder_rnn, zoneout_prob=0.1)
        """
        self.decoder_rnn = nn.LSTMCell(hp.chn.attention_rnn + self.encoder_embedding_dim,
                                       hp.chn.decoder_rnn, 1)
        self.mel_fc = nn.Linear(
            hp.chn.decoder_rnn + self.encoder_embedding_dim, hp.audio.n_mel_channels * hp.train.n_frames_per_step)
        self.prenet_f0 = ConvNorm(
            1, hp.prenet_f0_dim,
            kernel_size=hp.prenet_f0_kernel_size,
            padding=max(0, int(hp.prenet_f0_kernel_size / 2)),
            bias=False, stride=1, dilation=1)

    def get_go_frame(self, memory):
        return self.go_frame.expand(memory.size(0), self.hp.audio.n_mel_channels)

    def get_end_f0(self, f0s):
        B = f0s.size(0)
        dummy = Variable(f0s.data.new(B, 1, f0s.size(1)).zero_())
        return dummy

    def initialize(self, memory, mask):
        B, T, _ = memory.size()
        device = memory.device

        self.processed_memory = self.attention_layer.memory_layer(memory)
        self.attention_weights_cum = torch.zeros(B, T).to(device)
        self.memory = memory
        self.mask = mask


        attn_h = torch.zeros(B, self.hp.chn.attention_rnn).to(device)
        attn_c = torch.zeros(B, self.hp.chn.attention_rnn).to(device)
        dec_h = torch.zeros(B, self.hp.chn.decoder_rnn).to(device)
        dec_c = torch.zeros(B, self.hp.chn.decoder_rnn).to(device)

        prev_attn = torch.zeros(B, T).to(device)
        context = torch.zeros(B, self.encoder_embedding_dim).to(device)

        return attn_h, attn_c, dec_h, dec_c, prev_attn, context

    def decode(self, x, attn_h, attn_c, dec_h, dec_c, prev_attn, context, attention_weights=None):
        x = torch.cat((x, context), dim=-1)
        # [B, chn.prenet + (chn.encoder + chn.speaker)]
        attn_h, attn_c = self.attention_rnn(x, (attn_h, attn_c))
        # [B, chn.attention_rnn]
        attn_h = F.dropout(attn_h, 0.1, self.training)
        attn_c = F.dropout(attn_c, 0.1, self.training)

        attention_weights_cat = torch.cat(
            (prev_attn.unsqueeze(1),
             self.attention_weights_cum.unsqueeze(1)), dim=1)
        context, prev_attn = self.attention_layer(
            attn_h, self.memory, self.processed_memory,
            attention_weights_cat, self.mask, attention_weights)
        # context: [B, (chn.encoder + chn.speaker)], prev_attn: [B, T]

        self.attention_weights_cum += prev_attn
        x = torch.cat((attn_h, context), dim=-1)
        # [B, chn.attention_rnn + (chn.encoder + chn.speaker)]
        dec_h, dec_c = self.decoder_rnn(x, (dec_h, dec_c))
        # [B, chn.decoder_rnn]
        dec_h = F.dropout(dec_h, 0.1, self.training)
        dec_c = F.dropout(dec_c, 0.1, self.training)

        x = torch.cat((dec_h, context), dim=-1)
        # [B, chn.decoder_rnn + (chn.encoder + chn.speaker)]
        mel_out = self.mel_fc(x)
        # [B, audio.n_mel_channels]

        return mel_out, attn_h, attn_c, dec_h, dec_c, prev_attn, context

    def parse_decoder_inputs(self, decoder_inputs):
        """ Prepares decoder inputs, i.e. mel outputs
        PARAMS
        ------
        decoder_inputs: inputs used for teacher-forced training, i.e. mel-specs
        RETURNS
        -------
        inputs: processed decoder inputs
        """
        # (B, n_mel_channels, T_out) -> (B, T_out, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(1, 2)
        # (B, T_out, n_mel_channels) -> (T_out, B, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(0, 1)
        return decoder_inputs

    def parse_decoder_outputs(self, mel_outputs, alignments):
        # 'T' is T_dec.
        mel_outputs = torch.stack(mel_outputs, dim=0).transpose(0, 1).contiguous()
        mel_outputs = mel_outputs.view(
            mel_outputs.size(0), -1, self.n_mel_channels)
        mel_outputs = mel_outputs.transpose(1, 2)
        # mel: [T, B, M] -> [B, T, M] -> [B, M, T]
        alignments = torch.stack(alignments, dim=0).transpose(0, 1).contiguous()
        # align: [T_dec, B, T_enc] -> [B, T_dec, T_enc]

        return mel_outputs, alignments

    def forward(self, x, memory, f0s, memory_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, no_mask=False, tfrate=1.0):
        # x: mel spectrogram for teacher-forcing. [B, M, T].
        go_frame = self.get_go_frame(memory).unsqueeze(0)
        x = self.parse_decoder_inputs(x)  # [B, M, T] -> [B, T, M] -> [T, B, M]
        x = torch.cat((go_frame, x), dim=0)  # [T+1, B, M]
        x = self.prenet(x, prenet_dropout)

        f0_dummy = self.get_end_f0(f0s)
        f0s = torch.cat((f0s, f0_dummy), dim=2)
        f0s = F.relu(self.prenet_f0(f0s))
        f0s = f0s.permute(2, 0, 1)

        attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
            self.initialize(memory,
                            mask=None if no_mask else ~self.get_mask_from_lengths(memory_lengths))
        mel_outputs, alignments = [], []
        decoder_input = x[0]

        while len(mel_outputs) < x.size(0) - 1:
            decoder_input = torch.cat((decoder_input, f0s[len(mel_outputs)]), dim=1)
            mel_out, attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
                self.decode(decoder_input, attn_h, attn_c, dec_h, dec_c, prev_attn, context)

            alignments.append(prev_attn)
            mel_outputs.append(mel_out)

            if tfrate < random.random():
                decoder_input = self.prenet(mel_out, prenet_dropout)
            else:
                decoder_input = x[len(mel_outputs)]

        mel_outputs, alignments = self.parse_decoder_outputs(mel_outputs, alignments)
        mel_postnet = mel_outputs + self.postnet(mel_outputs)

        # DataParallel expects equal sized inputs/outputs, hence padding
        alignments = alignments.unsqueeze(0)
        alignments = F.pad(alignments, (0, max_input_len[0] - alignments.size(-1)), 'constant', 0)
        alignments = alignments.squeeze(0)

        mel_outputs, mel_postnet, alignments = \
            self.mask_output(mel_outputs, mel_postnet, alignments, output_lengths)
        return mel_outputs, mel_postnet, alignments

    def inference(self, memory, f0s, prenet_dropout):
        decoder_input = self.get_go_frame(memory)
        attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
            self.initialize(memory, mask=None)
        mel_outputs, alignments = [], []
        input_length = memory.size(1)
        position = np.arange(input_length)
        running_mean = 0.0
        u = 0
        st = 0
        while True:
            decoder_input = self.prenet(decoder_input, prenet_dropout)

            st = max(torch.argmax(prev_attn), st)
            mel_out, attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
                self.decode(decoder_input, attn_h, attn_c, dec_h, dec_c, prev_attn, context)

            if st >= torch.argmax(prev_attn):
                u += 1
            else:
                u = 0

            if u < 50:
                mel_outputs.append(mel_out)
                alignments.append(prev_attn)

            if torch.max(prev_attn) < 0.4:
                print('edited')
                attn_h -= attn_h
                attn_c -= attn_c
                dec_h -= dec_h
                dec_c -= dec_c
                idx = torch.argmax(prev_attn)
                prev_attn = prev_attn - prev_attn
                prev_attn[:, idx] = 1.0
                context = torch.bmm(prev_attn.unsqueeze(1), memory).squeeze(1)
            
            decoder_input = mel_out

            # use exponential moving average as a threshold for stopping the loop
            attention = prev_attn[0].cpu().detach().numpy()
            mean = sum(attention * position)
            running_mean = 0.9 * running_mean + 0.1 * mean

            if running_mean > input_length - 3.0:
                break
            elif len(mel_outputs) == 15000:
                print("Warning! Reached max decoder steps")
                break

        mel_outputs, alignments = self.parse_decoder_outputs(mel_outputs, alignments)
        mel_postnet = mel_outputs + self.postnet(mel_outputs)
        return mel_outputs, mel_postnet, alignments

    def inference_noattention(self, memory, f0s, attention_map, prenet_dropout=0.5):
        """ Decoder inference
        PARAMS
        ------
        memory: Encoder outputs

        RETURNS
        -------
        mel_outputs: mel outputs from the decoder
        gate_outputs: gate outputs from the decoder
        alignments: sequence of attention weights from the decoder
        """
        decoder_input = self.get_go_frame(memory)
        attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
            self.initialize(memory, mask=None)

        f0_dummy = self.get_end_f0(f0s)
        f0s = torch.cat((f0s, f0_dummy), dim=2)
        f0s = F.relu(self.prenet_f0(f0s))
        f0s = f0s.permute(2, 0, 1)

        mel_outputs, alignments = [], []
        for i in range(len(attention_map)):
            f0 = f0s[i]
            prev_attn = attention_map[i]
            decoder_input = torch.cat((self.prenet(decoder_input, prenet_dropout), f0), dim=1)
            mel_out, attn_h, attn_c, dec_h, dec_c, prev_attn, context = \
                self.decode(decoder_input, attn_h, attn_c, dec_h, dec_c, prev_attn, context, attention_weights=prev_attn)

            mel_outputs += [mel_out]
            alignments += [prev_attn]

            decoder_input = mel_out

        mel_outputs, alignments = self.parse_decoder_outputs(mel_outputs, alignments)
        mel_postnet = mel_outputs + self.postnet(mel_outputs)

        return mel_outputs, mel_postnet, alignments

    def get_mask_from_lengths(self, lengths, max_len=None):
        if max_len is None:
            max_len = torch.max(lengths).item()
        ids = torch.arange(0, max_len, out=torch.cuda.LongTensor(max_len))
        mask = (ids < lengths.unsqueeze(1))
        return mask

    def mask_output(self, mel_outputs, mel_postnet, alignments, output_lengths=None):
        if self.hp.train.mask_padding and output_lengths is not None:
            mask = ~self.get_mask_from_lengths(output_lengths, max_len=mel_outputs.size(-1))
            mask = mask.unsqueeze(1)  # [B, 1, T] torch.bool
            mel_outputs.masked_fill_(mask, 0.0)
            mel_postnet.masked_fill_(mask, 0.0)

        return mel_outputs, mel_postnet, alignments
