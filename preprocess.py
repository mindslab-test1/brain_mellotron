from concurrent.futures import ProcessPoolExecutor
from functools import partial
import numpy as np
from omegaconf import OmegaConf
import os
from scipy.io.wavfile import write, read
import torch
import glob
from scipy import interpolate
from yin import compute_yin
from random import shuffle


def get_f0(audio, sampling_rate=22050, frame_length=1024,
           hop_length=256, f0_min=100, f0_max=300, harm_thresh=0.1):
    f0, harmonic_rates, argmins, times = compute_yin(
        audio, sampling_rate, frame_length, hop_length, f0_min, f0_max,
        harm_thresh)
    pad = int((frame_length / hop_length) / 2)
    f0 = [0.0] * pad + f0 + [0.0] * pad

    f0 = np.array(f0, dtype=np.float32)
    return f0


def _process_speaker(filepath):
    min_tot = 1000
    max_tot = 0
    audio, sampling_rate = load_wav_to_torch(filepath)
    f0 = get_f0(audio.cpu().numpy(), 22050, 1024, 256, 80, 880, 0.25)
    min_f0 = np.min(f0[np.nonzero(f0)])
    max_f0 = f0.max()
    if min_tot > min_f0:
        min_tot = min_f0
    if max_tot < max_f0:
        max_tot = max_f0
    sum_over_frames = np.sum(f0[np.nonzero(f0)])
    n_frames = len(f0[np.nonzero(f0)])

    return round(min_tot), round(max_tot), sum_over_frames, n_frames


def write_metadata(metadata, out_file):
    with open(out_file, 'w', encoding='utf-8') as f:
        for m in metadata:
            if m is None:
                continue
            f.write('|'.join([str(x) for x in m]) + '\n')


def load_wav_to_torch(full_path):
    # scipy.wavefil.read does not take care of the case where wav is int or uint.
    # Thus, scipy.read is replaced with read_wav_np
    sampling_rate, data = read_wav_np(full_path)
    return torch.FloatTensor(data.astype(np.float32)), sampling_rate


def read_wav_np(path):
    try:
        sr, wav = read(path)
    except Exception as e:
        print(str(e) + path)
        return Exception

    if len(wav.shape) == 2:
        wav = wav[:, 0]

    if wav.dtype == np.int16:
        wav = wav / 32768.0
    elif wav.dtype == np.int32:
        wav = wav / 2147483648.0
    elif wav.dtype == np.uint8:
        wav = (wav - 128) / 128.0

    wav = wav.astype(np.float32)

    return sr, wav

hp = OmegaConf.load('config/config.yaml')
with open(os.path.join(hp.data.train_dir,hp.data.train_meta), 'r', encoding='utf-8') as g:
    data = g.readlines()
wavdir = [x.split('|')[0].strip() for x in data]
speaker = [x.split('|')[2].strip() for x in data]
speaker_dict = set()

speaker_dict = hp.data.speakers

n = len(speaker_dict)
print(speaker_dict)
speaker_to_idx = {spk: idx for idx, spk in enumerate(speaker_dict)}

mins = [1000. for i in range(n)]
maxs = [0. for i in range(n)]
means = [0. for i in range(n)]
frame_count = [0 for i in range(n)]
speaker_count = [0 for i in range(n)]

for i, fpath in enumerate(wavdir):
    if (speaker_count[speaker_to_idx[speaker[i]]] >= 10):
        continue
    speaker_count[speaker_to_idx[speaker[i]]] += 1
    aa, bb, cc, dd = _process_speaker(os.path.join(hp.data.train_dir, fpath))
    mins[speaker_to_idx[speaker[i]]] = min(mins[speaker_to_idx[speaker[i]]], aa)
    maxs[speaker_to_idx[speaker[i]]] = max(mins[speaker_to_idx[speaker[i]]], bb)
    means[speaker_to_idx[speaker[i]]] += cc
    frame_count[speaker_to_idx[speaker[i]]] += dd

result = []
for i in range(n):
    u = []
    u.append(speaker_dict[i])
    u.append(mins[i])
    u.append(maxs[i])
    u.append(means[i] / frame_count[i])
    result.append(u)

write_metadata(result, 'f0_merged.txt')
