import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize


def plot_alignments(alignment1, alignment2, transpose=True):
    if transpose:
        alignment1 = alignment1.T
        alignment2 = alignment2.T

    fig = plt.figure(figsize=(12, 7))
    
    plt.subplot(211)
    plt.imshow(alignment1, aspect='auto', origin='lower', interpolation='none',
        norm=Normalize(vmin=0.0, vmax=1.0))
    plt.ylabel('Encoder timestep')
    
    plt.subplot(212)
    plt.imshow(alignment2, aspect='auto', origin='lower', interpolation='none',
        norm=Normalize(vmin=0.0, vmax=1.0))
    plt.xlabel('Decoder timestep')
    plt.ylabel('Encoder timestep')

    plt.subplots_adjust(bottom=0.1, right=0.88, top=0.9)
    cax = plt.axes([0.9, 0.1, 0.02, 0.8])
    plt.colorbar(cax=cax)
    return fig

def plot_spectrograms(mel_pred, mel_postnet, mel_target):
    fig = plt.figure(figsize=(12, 9))
    
    plt.subplot(311)
    plt.imshow(mel_pred, aspect='auto', origin='lower', interpolation='none')
    plt.ylabel('before postnet')

    plt.subplot(312)
    plt.imshow(mel_postnet, aspect='auto', origin='lower', interpolation='none')
    plt.ylabel('after postnet')

    plt.subplot(313)
    plt.imshow(mel_target, aspect='auto', origin='lower', interpolation='none')
    plt.ylabel('target mel')
    plt.xlabel('time frames')
    
    plt.subplots_adjust(bottom=0.1, right=0.88, top=0.9)
    cax = plt.axes([0.9, 0.1, 0.02, 0.8])
    plt.colorbar(cax=cax)
    return fig

def plot_f0s(f0):
    fig, ax = plt.subplots(figsize=(12, 3))
    ax.scatter(range(len(f0)), f0, alpha=0.5,
               color='red', marker='+', s=1, label='f0')

    plt.xlabel("Frames")
    plt.ylabel("Frequency(-70Hz)")
    plt.tight_layout()

    fig.canvas.draw()

    return fig