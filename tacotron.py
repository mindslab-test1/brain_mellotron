import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import pytorch_lightning as pl
import random
import numpy as np
from omegaconf import OmegaConf

from datasets.text import Language
from modules import TextEncoder, TacotronDecoder, Audio2Mel, GST
from datasets import TextMelDataset, text_mel_collate
from utils.alignment_loss import GuidedAttentionLoss


class Tacotron(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.hparams = hparams  # used for pl
        hp = OmegaConf.load(hparams.config)
        self.hp = hp
        self.n_frames_per_step = hp.train.n_frames_per_step

        self.symbols = Language(hp.data.lang, hp.data.text_cleaners).get_symbols()
        self.symbols = ['"{}"'.format(symbol) for symbol in self.symbols]
        self.encoder = TextEncoder(hp.chn.encoder, hp.ker.encoder, hp.depth.encoder, len(self.symbols))
        self.speaker_embedding = nn.Embedding(len(hp.data.speakers), hp.chn.speaker)
        self.decoder = TacotronDecoder(hp)
        self.audio2mel = Audio2Mel(hp.audio.filter_length, hp.audio.hop_length, hp.audio.win_length,
                                   hp.audio.sampling_rate, hp.audio.n_mel_channels, hp.audio.mel_fmin,
                                   hp.audio.mel_fmax)
        self.gst = GST(hp)
        self.is_val_first = True
        self.attn_loss = GuidedAttentionLoss(1000, 0.25, 1.00025)

        self.collate_fn = text_mel_collate(hp.train.n_frames_per_step)

    def forward(self, text, mel_target, speakers, f0s, input_lengths, output_lengths, max_input_len,
                prenet_dropout=0.5, no_mask=False, tfrate=1.0):
        text_encoding = self.encoder(text, input_lengths)  # [B, T, chn.encoder]
        speaker_emb = self.speaker_embedding(speakers)  # [B, chn.speaker]
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)  # [B, T, chn.speaker]
        gst_emb = self.gst(mel_target, output_lengths)
        gst_emb = gst_emb.repeat(1, text_encoding.size(1), 1)
        decoder_input = torch.cat((text_encoding, gst_emb, speaker_emb), dim=2)
        mel_pred, mel_postnet, alignment = \
            self.decoder(mel_target, decoder_input, f0s, input_lengths, output_lengths, max_input_len,
                         prenet_dropout, no_mask, tfrate)
        return mel_pred, mel_postnet, alignment

    def inference(self, text, speakers, f0s, prenet_dropout=0.5):
        text_encoding = self.encoder.inference(text)
        speaker_emb = self.speaker_embedding(speakers)
        speaker_emb = speaker_emb.unsqueeze(1).expand(-1, text_encoding.size(1), -1)
        decoder_input = torch.cat((text_encoding, speaker_emb), dim=2)
        _, mel_postnet, alignment = \
            self.decoder.inference(decoder_input, prenet_dropout)
        return mel_postnet, alignment

    def inference_noattention(self, text, style_input, speaker_ids, f0s, attention_map):
        embedded_text = self.encoder.inference(text)
        embedded_speakers = self.speaker_embedding(speaker_ids)[:, None]
        if hasattr(self, 'gst'):
            if isinstance(style_input, int):
                query = torch.zeros(1, 1, self.gst.encoder.ref_enc.gru_size).cuda()
                GST = torch.tanh(self.gst.stl.embed)
                key = GST[style_input].unsqueeze(0).expand(1, -1, -1)
                embedded_gst = self.gst.stl.attention(query, key)
            else:
                embedded_gst = self.gst(style_input)

        embedded_speakers = embedded_speakers.repeat(1, embedded_text.size(1), 1)
        if hasattr(self, 'gst'):
            embedded_gst = embedded_gst.repeat(1, embedded_text.size(1), 1)
            encoder_outputs = torch.cat(
                (embedded_text, embedded_gst, embedded_speakers), dim=2)
        else:
            encoder_outputs = torch.cat(
                (embedded_text, embedded_speakers), dim=2)

        _, mel_postnet, alignments = self.decoder.inference_noattention(
            encoder_outputs, f0s, attention_map)

        return mel_postnet, alignments

    def training_step(self, batch, batch_idx):
        text, mel_target, speakers, f0_padded, input_lengths, output_lengths, max_input_len = batch
        mel_pred, mel_postnet, alignment = \
            self.forward(text, mel_target, speakers, f0_padded, input_lengths, output_lengths, max_input_len, prenet_dropout=0.5,
                         tfrate=self.hp.train.teacher_force.rate)

        loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target)
        attention_loss = self.attn_loss(alignment, input_lengths, output_lengths, self.global_step)

        self.logger.log_loss(loss, mode='train', step=self.global_step)
        self.logger.log_loss(attention_loss, mode='train.attention', step=self.global_step)

        return {'loss': loss  + attention_loss}

    def validation_step(self, batch, batch_idx):
        text, mel_target, speakers, f0_padded, input_lengths, output_lengths, max_input_len = batch

        mel_pred, mel_postnet, alignment = \
            self.forward(text, mel_target, speakers, f0_padded, input_lengths, output_lengths, max_input_len,
                         prenet_dropout=0.5, tfrate=1.0)

        loss = F.mse_loss(mel_pred, mel_target) + F.mse_loss(mel_postnet, mel_target)

        if self.is_val_first:  # plot alignment, character embedding
            self.is_val_first = False
            self.logger.log_figures(mel_pred, mel_postnet, mel_target, alignment, f0_padded, self.global_step)
            self.logger.log_embedding(self.symbols, self.encoder.embedding.weight, self.global_step)

        return {'loss': loss}

    def validation_end(self, outputs):
        loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.logger.log_loss(loss, mode='val', step=self.global_step)
        self.is_val_first = True

        #torch.save(self.encoder.state_dict(), '/home/kwkim/dcatron_drop/encoder_state.ckpt')
        #torch.save(self.teacher.state_dict(), '/home/kwkim/dcatron_drop/decoder_state.ckpt')
        return {'val_loss': loss}

    def configure_optimizers(self):
        learnable_params = self.parameters()
        return torch.optim.Adam(
            learnable_params,
            lr=self.hp.train.adam.lr,
            weight_decay=self.hp.train.adam.weight_decay,
        )

    def lr_lambda(self, step):
        progress = (step - self.hp.train.decay.start) / (self.hp.train.decay.end - self.hp.train.decay.start)
        return self.hp.train.decay.rate ** np.clip(progress, 0.0, 1.0)

    def optimizer_step(self, epoch_nb, batch_nb, optimizer, optimizer_idx, second_order_closure):
        lr_scale = self.lr_lambda(self.global_step)
        for pg in optimizer.param_groups:
            pg['lr'] = lr_scale * self.hp.train.adam.lr

        optimizer.step()
        optimizer.zero_grad()

        self.logger.log_learning_rate(lr_scale * self.hp.train.adam.lr, self.global_step)

    def train_dataloader(self):
        trainset = TextMelDataset(self.hp, self.hp.data.train_dir, self.hp.data.train_meta, True)
        return DataLoader(trainset, batch_size=self.hp.train.batch_size, shuffle=True,
                          num_workers=self.hp.train.num_workers,
                          collate_fn=self.collate_fn, pin_memory=True, drop_last=False)

    def val_dataloader(self):
        valset = TextMelDataset(self.hp, self.hp.data.val_dir, self.hp.data.val_meta, False)
        return DataLoader(valset, batch_size=self.hp.train.batch_size, shuffle=False,
                          num_workers=self.hp.train.num_workers,
                          collate_fn=self.collate_fn, pin_memory=False, drop_last=False)
