import os
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from argparse import ArgumentParser
from omegaconf import OmegaConf

from utils.utils import get_commit_hash
from tacotron import Tacotron
from utils.loggers import TacotronLogger
import torch


def main(args):
    model = Tacotron(args)
    hp = OmegaConf.load(args.config)

    save_path = os.path.join(hp.log.chkpt_dir, args.name)
    os.makedirs(save_path, exist_ok=True)

    checkpoint_callback = ModelCheckpoint(
        filepath=save_path,
        monitor='val_loss',
        verbose=True,
        save_top_k=args.save_top_k,  # save all
        prefix=get_commit_hash(),
    )

    tb_logger = TacotronLogger(
        save_dir=hp.log.log_dir,
        name=args.name,
    )
    '''
    state = torch.load('decoder_state.ckpt', map_location=lambda storage, loc: storage)
    model.teacher.load_state_dict(state)
    del state
    state = torch.load('encoder_state.ckpt', map_location=lambda storage, loc: storage)
    model.encoder.load_state_dict(state)
    del state
    '''
    state = torch.load('mellotron_libritts.pt', map_location=lambda storage, loc: storage)
    pretrained_dict = state['state_dict']
    new_model_dict = model.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if(k in new_model_dict and new_model_dict[k].size() == v.size())}
    for k,v in pretrained_dict.items():
        if k in new_model_dict and new_model_dict[k].size() == v.size():
            print(k)
    new_model_dict.update(pretrained_dict)
    model.load_state_dict(new_model_dict)
    del state
    
    trainer = Trainer(
        logger=tb_logger,
        early_stop_callback=None,
        checkpoint_callback=checkpoint_callback,
        default_save_path=save_path,
        gpus=-1 if args.gpus is None else args.gpus,
        distributed_backend='ddp',
        num_sanity_val_steps=1,
        resume_from_checkpoint=args.checkpoint_path,
        gradient_clip_val=hp.train.grad_clip,
        fast_dev_run=args.fast_dev_run,
        check_val_every_n_epoch=args.val_epoch,
        progress_bar_refresh_rate=1,
        max_epochs=10000,
    )
    trainer.fit(model)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="path of configuration yaml file")
    parser.add_argument('-g', '--gpus', type=str, default=None,
                        help="Number of gpus to use (e.g. '0,1,2,3'). Will use all if not given.")
    parser.add_argument('-n', '--name', type=str, required=True,
                        help="Name of the run.")
    parser.add_argument('-p', '--checkpoint_path', default=None, type=str,
                        help="path of checkpoint for resuming")
    parser.add_argument('-s', '--save_top_k', type=int, default=-1,
                        help="save top k checkpoints, default(-1): save all")
    parser.add_argument('-f', '--fast_dev_run', type=bool, default=False,
                        help="fast run for debugging purpose")
    parser.add_argument('--val_epoch', type=int, default=1,
                        help="run val loop every * training epochs")
    args = parser.parse_args()

    main(args)
